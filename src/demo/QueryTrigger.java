package demo;

import oo2apl.agent.Trigger;

public final class QueryTrigger implements Trigger {
	public final String query;
	
	public QueryTrigger(final String query){
		this.query = query; 
	}
	
	public final String getQuery(){
		return this.query;
	}
}
