package demo;

import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import oo2apl.agent.ExternalProcessToAgentInterface;
import oo2apl.defaults.messenger.DefaultMessenger;
import oo2apl.platform.AdminToPlatformInterface;
import oo2apl.platform.Platform;
/**
 * This is a small demo that shows how an agent can interact with tuProlog to enable Prolog capabilities. 
 * 
 * You have to import OO2APL and the tuProlog capability and the tuprolog.jar file in order to run the demo.
 *  
 * Whilst running you can query the agent with the console.
 * 
 * @author Bas Testerink
 *
 */
public final class PrologDemoMain {
	// User input
	private final Scanner scanner;
	private final ExecutorService executor;
	
	// The agent platform
	private final AdminToPlatformInterface platform;
	private final ExternalProcessToAgentInterface agent;
	
	public PrologDemoMain(){ 
		// Make the agent
		this.platform = Platform.newPlatform(1, new DefaultMessenger());
		this.agent = platform.newAgent(new DemoAgent());

		// Make the console connection
		this.scanner = new Scanner(System.in);
		this.executor = Executors.newSingleThreadExecutor(); 
		this.executor.submit(() -> { 
			System.out.println("Type a query or type \"halt\" to end.\r\n");
			boolean exit = false; 
			while(!exit){ 
				String input = this.scanner.nextLine(); 
				try {
					if(input.equalsIgnoreCase("halt"))
						exit = true;
					else handleInput(input); 
				} catch (Exception e) { 
					exit = true;
					e.printStackTrace();
				}
			}
			this.executor.shutdown();
			this.platform.haltPlatform();
		});
	}
	
	/** Handle the input from the console, which is sent to the agent as a query. */
	private final void handleInput(final String input){
		System.out.println(">"+input);
		if(input.endsWith("."))
			this.agent.addExternalTrigger(new QueryTrigger(input));
		else 
			this.agent.addExternalTrigger(new QueryTrigger(input+"."));
	}

	public static final void main(final String[] args){
		new PrologDemoMain();
	}
}
