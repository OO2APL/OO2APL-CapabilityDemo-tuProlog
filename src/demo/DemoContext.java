package demo;

import java.util.UUID;

import oo2apl.agent.Context;

public final class DemoContext implements Context {
	private UUID myPrologEngine;
	
	public final void setPrologEngine(final UUID id){
		this.myPrologEngine = id;
	}
	
	public final UUID getPrologEngine(){
		return this.myPrologEngine;
	}
}
