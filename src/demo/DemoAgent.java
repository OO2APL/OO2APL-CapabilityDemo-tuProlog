package demo;

import java.io.IOException;
import java.util.UUID;

import alice.tuprolog.InvalidTheoryException;
import alice.tuprolog.MalformedGoalException;
import alice.tuprolog.SolveInfo;
import oo2apl.agent.AgentBuilder;
import oo2apl.agent.AgentContextInterface;
import oo2apl.agent.PlanToAgentInterface;
import oo2apl.agent.Trigger; 
import oo2apl.plan.PlanExecutionError;
import oo2apl.plan.builtin.RunOncePlan;
import oo2apl.plan.builtin.SubPlanInterface;
import tuPrologCapability.TuPrologCapability;
import tuPrologCapability.TuPrologContext; 

/**
 * The demo agent can receive external triggers that contain queries. The agent 
 * reacts to this triggers by executing the query (on mybeliefbase.txt) and showing 
 * the result.
 * 
 * @author Bas Testerink
 *
 */
public final class DemoAgent extends AgentBuilder {
	
	public DemoAgent(){
		// Enable the usage of prolog
		super.include(new TuPrologCapability()); 
		super.addContext(new DemoContext()); // For storing the prolog engine ID
		addInitialPlan(); 
		super.addExternalTriggerPlanScheme(DemoAgent::handleQueryScheme);
	}
	
	/** A query is handled by executing it and showing the first result. */
	private static final SubPlanInterface handleQueryScheme(final Trigger trigger, final AgentContextInterface contextInterface){
		if(trigger instanceof QueryTrigger){
			return (planInterface)->{
				UUID prologID = planInterface.getContext(DemoContext.class).getPrologEngine();
				TuPrologContext prolog = planInterface.getContext(TuPrologContext.class);
				SolveInfo result;
				try {
					result = prolog.queryFirstResult(prologID, ((QueryTrigger)trigger).getQuery());
					System.out.println(result.toString());
				} catch (MalformedGoalException e) { 
					e.printStackTrace();
				}
			};
		} else return SubPlanInterface.UNINSTANTIATED;
	}
	
	/** Add the initial plan, which is to consult the belief base text file. */
	private final void addInitialPlan(){ 
		super.addInitialPlan(new RunOncePlan(){
			public void executeOnce(PlanToAgentInterface planInterface) throws PlanExecutionError {
				// Add the prolog base
				TuPrologContext prolog = planInterface.getContext(TuPrologContext.class);
				UUID prologID;
				try {
					prologID = prolog.newPrologEngine("mybeliefbase.txt");
					planInterface.getContext(DemoContext.class).setPrologEngine(prologID); 
				} catch (InvalidTheoryException | IOException e) { 
					e.printStackTrace();
				} 
			}
		});
	}   
}
